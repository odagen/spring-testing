package com.epam.rd.testing.controller;

import com.epam.rd.testing.SpringIntegrationTest;
import org.junit.Test;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.transaction.annotation.Transactional;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@Transactional
public class SecretControllerIT extends SpringIntegrationTest {

    @Test
    public void shouldGetRedirectionToLoginPage() throws Exception {
        mockMvc.perform(get("/secret")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isFound());
    }

    @WithMockUser("USER")
    @Test
    public void shouldGetForbiddenResponse() throws Exception {
        mockMvc.perform(get("/secret")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isForbidden());
    }

    @WithMockUser(value = "ADMIN", roles = {"ADMIN"})
    @Test
    public void shouldGetSecretResource() throws Exception {
        mockMvc.perform(get("/secret")
                .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk());
    }
}
