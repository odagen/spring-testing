package com.epam.rd.testing.controller;

import com.epam.rd.testing.service.TransactionService;
import com.epam.rd.testing.service.dto.TransactionResponseDto;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.List;

import static com.epam.rd.testing.utils.TestDataGenerator.generateResponseTransactionDtos;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.BDDMockito.given;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Example of MVC test using spring-boot WebMvcTest slice
 */
@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CurrencySaleController.class)
public class CurrencySaleControllerTest {

    private MockMvc mockMvc;

    @Autowired
    private WebApplicationContext webApplicationContext;

    @MockBean
    private TransactionService transactionService;

    @Before
    public void setup() {
        mockMvc = MockMvcBuilders
                .webAppContextSetup(webApplicationContext)
                .apply(springSecurity())
                .build();
    }

    @WithMockUser
    @Test
    public void getAllTransactionsShouldReturnTransactionsDetails() throws Exception {
        List<TransactionResponseDto> inputTransactionDtos = generateResponseTransactionDtos(2);

        given(transactionService.findAllTransactions()).willReturn(inputTransactionDtos);

        mockMvc.perform(get("/transactions")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(2)))
                .andExpect(jsonPath("$[0].profileId", Matchers.is(inputTransactionDtos.get(0).getProfileId())))
                .andExpect(jsonPath("$[1].profileId", Matchers.is(inputTransactionDtos.get(1).getProfileId())))
                .andExpect(jsonPath("$[0].currency", Matchers.is(inputTransactionDtos.get(0).getCurrency())))
                .andExpect(jsonPath("$[1].currency", Matchers.is(inputTransactionDtos.get(1).getCurrency())))
                .andExpect(jsonPath("$[0].transactionStatus", Matchers.is(inputTransactionDtos.get(0).getTransactionStatus())))
                .andExpect(jsonPath("$[1].transactionStatus", Matchers.is(inputTransactionDtos.get(1).getTransactionStatus())));
    }
}
