//package com.epam.rd.testing.controller;
//
//import org.junit.Before;
//import org.junit.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.http.MediaType;
//import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
//import org.springframework.test.web.servlet.setup.MockMvcBuilders;
//import org.springframework.web.context.WebApplicationContext;
//
//import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
//import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
//
//public class SecretControllerTest {
//
//    @Autowired
//    private WebApplicationContext webApplicationContext;
//
//    @Before
//    public void startMocks() {
//        mockMvc = MockMvcBuilders
//                .webAppContextSetup(webApplicationContext)
//                .apply(SecurityMockMvcConfigurers.springSecurity())
//                .alwaysDo(print())
//                .build();
//    }
//
//    @Test
//    public void getAllTransactionsShouldReturnTransactionsDetails() throws Exception {
//        mockMvc.perform(get("/secret")
//                .accept(MediaType.APPLICATION_JSON_VALUE))
//                .andExpect(status().isOk());
//    }
//}
