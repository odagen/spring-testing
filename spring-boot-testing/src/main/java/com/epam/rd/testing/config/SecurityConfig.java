package com.epam.rd.testing.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;


@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    enum Roles {
        GUEST,
        USER,
        ADMIN
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests().antMatchers("/").permitAll()
                .and()
                .authorizeRequests().antMatchers("/secret").hasRole(Roles.ADMIN.name())
                .and()
                .authorizeRequests().antMatchers("/transactions/**").hasAnyRole(Roles.ADMIN.name(), Roles.USER.name())
                .anyRequest().authenticated()
                .and()
                .formLogin();
    }

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin").password(passwordEncoder().encode("nimda")).roles(Roles.ADMIN.name())
                .and()
                .withUser("user").password(passwordEncoder().encode("resu")).roles(Roles.USER.name())
                .and()
                .withUser("guest").password(passwordEncoder().encode("tseug")).roles(Roles.GUEST.name());
    }


    @Bean
    public PasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }
}




