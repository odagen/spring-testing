package com.epam.rd.testing.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path = "/secret")
public class SecretController {

    @GetMapping
    public ResponseEntity<String> getSecret() {
        return ResponseEntity.ok("!@#$s234rwef@#$23d");
    }

}
