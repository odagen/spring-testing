package com.epam.rd.testing.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/secret")
public class SecretController {

    @GetMapping
    public String getSecret(Model model) {
        model.addAttribute("secret", "!@#$s234rwef@#$23d");

        return "secret";
    }

}
