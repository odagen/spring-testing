package com.epam.rd.testing;

import org.apache.catalina.Context;
import org.apache.catalina.WebResourceRoot;
import org.apache.catalina.startup.Tomcat;
import org.apache.catalina.webresources.DirResourceSet;
import org.apache.catalina.webresources.StandardRoot;
import org.apache.tomcat.util.scan.StandardJarScanner;

import java.io.File;
import java.io.IOException;

public class Application {
    private static final int PORT = 8091;
    private static final String DOC_BASE = "src/main/webapp/";

    public static void main(String[] args) throws Exception {
        Tomcat tomcat = new Tomcat();
        tomcat.setBaseDir(createTempDir());
        tomcat.getHost().setAppBase(new File(DOC_BASE).getAbsolutePath());
        tomcat.getConnector().setPort(PORT);

        Context context = tomcat.addWebapp("", new File(DOC_BASE).getAbsolutePath());

//        File additionWebInfClasses = new File("target/classes");
//        WebResourceRoot resources = new StandardRoot(context);
//        resources.addPreResources(new DirResourceSet(resources, "/WEB-INF/classes",
//                additionWebInfClasses.getAbsolutePath(), "/"));
//        context.setResources(resources);

        StandardJarScanner jarScanner = new StandardJarScanner();
        jarScanner.setScanManifest(false);
        context.setJarScanner(jarScanner);

        tomcat.start();
        tomcat.getServer().await();
    }

    private static String createTempDir() {
        try {
            File tempDir = File.createTempFile("tomcat.", "." + PORT);
            tempDir.delete();
            tempDir.mkdir();
            tempDir.deleteOnExit();
            return tempDir.getAbsolutePath();
        } catch (IOException ex) {
            throw new RuntimeException("Unable to create tempDir. java.io.tmpdir is set to "
                    + System.getProperty("java.io.tmpdir"), ex);
        }
    }
}