package com.epam.rd.testing.controller;

import com.epam.rd.testing.AbstractBaseSpringTest;
import org.junit.Test;
import org.springframework.http.MediaType;

import static org.hamcrest.CoreMatchers.is;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.user;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.model;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;

public class SecretControllerIT extends AbstractBaseSpringTest {

    @Test
    public void shouldGetRedirectToLoginPage() throws Exception {
        mockMvc.perform(get("/secret")
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isFound());
    }

    @Test
    public void shouldGetForbiddenResponse() throws Exception {
        mockMvc.perform(get("/secret")
                .with(user("user").password("resu").roles("USER"))
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isForbidden());
    }

    @Test
    public void shouldGetSecretResource() throws Exception {
        mockMvc.perform(get("/secret")
                .with(user("nimda").password("123123sfdf").roles("ADMIN"))
                .accept(MediaType.TEXT_HTML))
                .andExpect(status().isOk())
                .andExpect(view().name("secret"))
                .andExpect(model().attribute("secret", is("!@#$s234rwef@#$23d")));
    }
}
